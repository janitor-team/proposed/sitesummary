DESTDIR = 

prefix       = /usr
sbindir      = $(prefix)/sbin
datadir      = $(prefix)/share
libdir       = $(prefix)/lib
cgibindir    = $(libdir)/cgi-bin
pkgdatadir   = $(datadir)/sitesummary
pkgdir       = $(libdir)/sitesummary
collectordir = $(pkgdir)/collect.d
perldir      = $(datadir)/perl5
pkgvardir    = /var/lib/sitesummary
nagiosplugdir= $(pkgdir)/nagios-plugins

INSTALL      = install
INSTALL_DATA = $(INSTALL) -m 644

COLLECTORS = \
	collect.d/debian \
	collect.d/debian-edu \
	collect.d/ipmi \
	collect.d/nagios \
	collect.d/system \
	collect.d/siteinfo

SUMMARYSCRIPTS = \
	agesinceseen-summary \
	site-summary \
	hardware-model-summary \
	hostclass-summary \
	kernelversion-summary \
	debian_edu-summary

all:

install: install-server install-client

install-server:
	$(INSTALL) -d $(DESTDIR)$(cgibindir)
	$(INSTALL) sitesummary-collector.cgi $(DESTDIR)$(cgibindir)

	$(INSTALL) -d $(DESTDIR)$(sbindir)
	for f in sitesummary-makewebreport sitesummary-nodes sitesummary-update-munin sitesummary-update-nagios ; do \
		$(INSTALL) $$f $(DESTDIR)$(sbindir) ; \
	done

	$(INSTALL) -d $(DESTDIR)$(perldir)
	$(INSTALL) -d $(DESTDIR)$(pkgdir)
	$(INSTALL_DATA) SiteSummary.pm $(DESTDIR)$(perldir)
	$(INSTALL) $(SUMMARYSCRIPTS) $(DESTDIR)$(pkgdir)/

	$(INSTALL) expire-entry $(DESTDIR)$(pkgdir)/

	$(INSTALL) -d $(DESTDIR)/etc/apache2/conf-available
	$(INSTALL_DATA) apache.conf $(DESTDIR)/etc/apache2/conf-available/sitesummary.conf

	$(INSTALL) -o www-data -d $(DESTDIR)$(pkgvardir)/entries
	$(INSTALL) -o www-data -d $(DESTDIR)$(pkgvardir)/tmpstorage
	$(INSTALL) -d $(DESTDIR)$(pkgvardir)/www

	$(INSTALL) -d $(DESTDIR)/usr/share/munin/plugins/.
	$(INSTALL) munin-plugin $(DESTDIR)/usr/share/munin/plugins/sitesummary_sites
	$(INSTALL) munin-plugin-agesinceseen \
		$(DESTDIR)/usr/share/munin/plugins/sitesummary_agesinceseen

	$(INSTALL) -d $(DESTDIR)/etc/icinga
	$(INSTALL_DATA) nagios.cfg \
		$(DESTDIR)/etc/icinga/sitesummary.cfg
	$(INSTALL_DATA) nagios-templates.cfg \
		$(DESTDIR)/etc/icinga/sitesummary-templates.cfg
	$(INSTALL_DATA) nagios-template-contacts.cfg \
		$(DESTDIR)/etc/icinga/sitesummary-template-contacts.cfg
	$(INSTALL) -d $(DESTDIR)/etc/nagios
	$(INSTALL_DATA) nagios-nrpe-commands.cfg \
		$(DESTDIR)/etc/nagios/sitesummary-nrpe-commands.cfg

install-client:
	$(INSTALL) -d $(DESTDIR)$(sbindir)
	$(INSTALL) sitesummary-client sitesummary-upload $(DESTDIR)$(sbindir)
	$(INSTALL) -d $(DESTDIR)$(pkgdatadir)
	$(INSTALL_DATA) sitesummary-client.conf $(DESTDIR)$(pkgdatadir)/

	$(INSTALL) -d $(DESTDIR)$(collectordir)
	for collector in $(COLLECTORS) ; do \
		$(INSTALL) $$collector $(DESTDIR)$(collectordir); \
	done

	$(INSTALL) -d $(DESTDIR)$(nagiosplugdir)
	for plugin in nagios-plugins/* ; do \
		$(INSTALL) $$plugin $(DESTDIR)$(nagiosplugdir); \
	done

clean:
	$(RM) *~ */*~
distclean: clean
