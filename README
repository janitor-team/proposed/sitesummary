sitesummary
===========

More information is available from
<URL: http://wiki.debian.org/DebianEdu/HowTo/SiteSummary >.

Sitesummary generated Munin configuraiton
-----------------------------------------

Sitesummary can be used to generate Munin configuration to collect
statistics from all sitesummary clients with the Munin packages
installed.  To enable it, install the munin package and make sure
/etc/sitesummary/collector.cfg include 'MUNINDIR=/etc/munin' to get
sitesummary to replace the default munin configuration with the
generated one.  Running these commands as root normally does the
trick.

  aptitude install munin munin-node
  echo MUNINDIR=/etc/munin >> /etc/sitesummary/collector.cfg
  sitesummary-client
  /etc/cron.daily/sitesummary

These steps can be preseeded during installation, here is an example
to enable Munin autoconfig on the sitesummary collector

  sitesummary sitesummary/replace-munin-config boolean true

Sitesummary generated Icinga configuration
------------------------------------------

Since version 0.0.51, there is code in sitesummary to generate Icinga
configuration based on the collected information.  To enable this,
install icinga and monitoring-plugins-standard, edit /etc/default/icinga
to include 'NAGIOSCFG=/etc/icinga/sitesummary.cfg' and run the
sitesummary cron job.  Running these commands as root normally does
the trick.

  aptitude install icinga monitoring-plugins-standard
  echo 'NAGIOSCFG="/etc/icinga/sitesummary.cfg"' >> /etc/default/icinga
  sitesummary-client
  /etc/cron.daily/sitesummary
  service icinga restart

The current version of the code run all Icinga checks locally, so it
only work on a single machine.  The plan is to change this to use NRPE
to work with a network of machines.

On the client side, add
'include=/etc/nagios/sitesummary-nrpe-commands.cfg' and
'dont_blame_nrpe=1' in /etc/nagios/nrpe_local.cfg to enable the
sitesummary checks.  To make sure the Icinga server can contact nrpe,
it might be required to list the server address using the
allowed_hosts option.  Running these commands as root normally does
the trick.

  aptitude install nagios-nrpe-server monitoring-plugins-standard
  cat > /etc/nagios/nrpe.d/sitesummary-nrpe.cfg <<EOF
allowed_hosts=myserver
dont_blame_nrpe=1
include=/etc/nagios/sitesummary-nrpe-commands.cfg
EOF
  sitesummary-client

These steps can be preseeded during installation, here is an example
to enable Icinga autoconfig on the sitesummary collector and allow it
to query Nagios NRPE on the clients.

  sitesummary sitesummary/enable-nagios-config boolean true

  sitesummary-client sitesummary-client/nagios-server string sitesummary
  sitesummary-client sitesummary-client/enable-nagios-nrpe-config boolean true

Handling hosts behind VPN connections
-------------------------------------

To handle hosts with hostnames missing in DNS or IP adresses not
available directly from the Icinga and Munin server, a mechanism to
map entries to new hostnames is provided.

To enable it, create a file /etc/sitesummary/hostmap or a file in
/etc/sitesummary/hostmap.d/ with content like this

  ether-AA:BB:CC:DD:EE:FF new-name

Empty lines are ignored.  The # character indicate the start of a
comment.  Entries in /etc/sitesummary/hostmap.d/ override entries in
/etc/sitesummary/hostmap, and the entries in hostmap.d/ are read in
alphabetic order, where the last entry take effect.

The ether-* name is the host entry name from
/var/lib/sitesummary/entries/, and the new-name entry is the hostname
to use instead of the value in system/hostname.

For this to work, the new-name entry must map to a IP address, either
in DNS via an entry in /etc/hosts.

Ignoring specific Icinga checks for some hosts
----------------------------------------------

Some times a machine have a package installed that cause some Icinga
test to be enabled, but the service provided by the package is
disabled and the check always fail.  In this case, the Icinga test can
be disabled using the /etc/sitesummary/nagiosignore file.

For example, to disable the DHCP check for a given host, add the
hostname and the Icinga check description to
/etc/sitesummary/nagiosignore like this:

  some.nagios.host.com process - dhcpd3

The Icinga description can be found in
/var/lib/sitesummary/nagios-generated.cfg.

Design draft
------------
system to collect key info about all the machines on a site, to help
the sysadmin keep track of a lot of hosts.

Should be capable of handling both thin clients and "real machines"

 - two package, one server sitesummary and one client sitesummary-client

 - the client submits information to the server using HTTP put.  It
   include the list of files it intend to submit, to detect deleted
   files.

 - the HTTP connection is either using SSL, or the file(s) transfered
   are GPG encrypted.  (figure out key exchange system)

 - the client submit a set of files and output from commands with hw
   information etc (need plugin system to make it submit more files)

    - lspci
    - lsusb
    - dmidecode
    - uname -s / -m / -r / -v / -o
    - ifconfig -a

    - public ssh host key
    - /etc/debian_version
    - dpkg -l
    - /etc/apt/sources.list
    - /etc/fstab
    - /proc/cpuinfo
    - /proc/meminfo

 - the server track changes done to the files (some version control
   system like rcs?) on each host, and make summary report on the
   number of individual machines reporting to the server

 - how do we identify machines?  MAC address could work (ip -s -f link
   maddr - which when several interfaces?).  IP addess will not work
   for thin clients on private networks behind two different servers.
   the linux hostid command is only using IP address and is useless.
   Can not generate random key stored on the file system, as this
   would give all thin clients on a server the same ID.  dmidecode
   serial number might work for some models, but others have the same
   serial number on several machines.

 - ideas for use of this info

   - machine count, grouped by os/kernel/etc

   - can generate a list of host keys for distribution to the clients

   - can generate dhcp MAC<->IP mapping

   - can generate lts.conf files for the LTSP clients

The server can either be a CGI script, or listen on some port on its
own.

The client should be a platform independent script with as few
dependencies as possible, to make it lightweight and easy to install
on all machines.  (perl, python, /bin/sh?)

Should the client know which files to report, or should the server be
able to affect it?  Can cfengine, icinga or munin be used for this
instead?
