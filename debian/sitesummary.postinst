#!/bin/sh

set -e

# Source debconf library.
. /usr/share/debconf/confmodule

test_nagios() {
    if grep -q '^NAGIOSCFG="/etc/icinga/sitesummary.cfg"$' "$config"; then
	:
    else
	db_get sitesummary/enable-nagios-config
	if [ true = "$RET" ] ; then
	    mkdir -p $(dirname $config)
	    echo 'NAGIOSCFG="/etc/icinga/sitesummary.cfg"' \
		>> /etc/default/icinga
	    if [ ! -f /var/lib/sitesummary/nagios-generated.cfg ] ; then
		# Create dummy file to make sure nagios will start
		# on first boot if sitesummary is installed using
		# debian-installer
		cat > /var/lib/sitesummary/nagios-generated.cfg <<EOF
define host {
        use       server-host
        host_name localhost
        address   localhost
}

define service {
        use       server-service
        host_name localhost
        service_description ping
        check_command       check_ping!100.0,20%!500.0,60%
}
EOF
	    fi
	fi
    fi
}

#DEBHELPER#

case "$1" in
    configure)
        # If the config file is missing, check debconf to see if the
        # munin configuration feature should be enabled.  This hidden
        # debconf question allow preseeding during installation.
	config=/etc/sitesummary/collector.cfg
	if [ ! -f $config ] ; then
	    db_get sitesummary/replace-munin-config
	    if [ true = "$RET" ] ; then
		mkdir -p /etc/sitesummary
		echo 'MUNINDIR=/etc/munin' > $config
	    fi
	fi

	config=/etc/default/icinga
	if [ -f "$config" ] ; then
	    test_nagios
	fi

        # Generate the web page at install time
	[ -f /var/lib/sitesummary/www/index.html ] || \
	    /etc/cron.daily/sitesummary

        # Close debconf file handles before restarting Apache
        db_stop

        # Enable it on fresh installations as before Apache 2.4.  Check for
        # cgi.load existence to avoid trying to configure when installed after
        # apache2 is unpacked but not yet configured (bug #760084).
        if [ -z "$2" ] && \
            [ -e /etc/apache2/mods-available/cgi.load ] && \
            [ -e /usr/share/apache2/apache2-maintscript-helper ] ; then
            . /usr/share/apache2/apache2-maintscript-helper
            apache2_invoke enmod cgi.load
            apache2_invoke enconf sitesummary.conf
        fi

        # Make sure the cgi script can write to the storage area
	chown www-data /var/lib/sitesummary/tmpstorage \
	    /var/lib/sitesummary/entries

	;;

  triggered)

        if [ -e /usr/share/apache2/apache2-maintscript-helper ] ; then
            . /usr/share/apache2/apache2-maintscript-helper
            apache2_invoke enmod cgi.load
            apache2_invoke enconf sitesummary.conf
        fi

        ;;

    abort-upgrade|abort-remove|abort-deconfigure)

	;;

    *)
        echo "postinst called with unknown argument \`$1'" >&2
        exit 1
    ;;
esac

exit 0
