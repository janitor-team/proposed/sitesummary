Source: sitesummary
Section: misc
Priority: optional
Maintainer: Debian Edu Developers <debian-edu@lists.debian.org>
Uploaders: Petter Reinholdtsen <pere@debian.org>
 , Holger Levsen <holger@debian.org>
 , Mike Gabriel <sunweaver@debian.org>
 , Dominik George <nik@naturalnet.de>
 , Wolfgang Schweer <wschweer@arcor.de>
Build-Depends: debhelper-compat (= 13)
Standards-Version: 4.5.1
Rules-Requires-Root: binary-targets
Homepage: https://wiki.debian.org/DebianEdu/HowTo/SiteSummary
Vcs-Browser: https://salsa.debian.org/debian-edu/upstream/sitesummary
Vcs-Git: https://salsa.debian.org/debian-edu/upstream/sitesummary.git

Package: sitesummary
Architecture: all
Pre-Depends: ${misc:Pre-Depends}
Depends: ${misc:Depends}, ${perl:Depends}, gnupg, net-tools, libcgi-pm-perl
Recommends: apache2 | httpd, sitesummary-client
Suggests: munin, munin-node, icinga2, monitoring-plugins-standard, ldap-utils, cups-client
Description: Generate site summary of submitting hosts (server part)
 The sitesummary system makes it easier to keep track of a lot of
 machines, by allowing each machine to report their existence once a
 day to a central collector, and using this collector to make summary
 reports about the hosts.
 .
 This package is the server part, with the collector and report
 scripts.

Package: sitesummary-client
Architecture: all
Depends: ${misc:Depends}, ${perl:Depends}, lsb-base (>= 3.0-6), gnupg, net-tools
Recommends: cron, dmidecode, pciutils, usbutils, lsscsi, iproute2, hdparm
Suggests: sitesummary, munin-node, nagios-nrpe-server, monitoring-plugins-standard, cdpr, ipmitool, zfsutils-linux
Description: Generate site summary of submitting hosts (client part)
 The sitesummary system makes it easier to keep track of a lot of
 machines, by allowing each machine to report their existence once a
 day to a central collector, and using this collector to make summary
 reports about the hosts.
 .
 This package is the client part, reporting in to the server after
 boot and once a day.
